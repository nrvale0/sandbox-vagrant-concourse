# -*- mode: ruby -*-

Vagrant.require_version ">= 2.2.5"
VAGRANTFILE_API_VERSION = "2"

ENV['VAGRANT_NO_COLOR'] = 'true'

ENV['VAGRANT_EXPERIMENTAL'] = "disks"

Vagrant.configure("2") do |config|
  config.vm.boot_timeout = 600 # because Ubuntu doesn't like ttyS0?
  config.vm.box = "ubuntu/focal64"
  config.vm.hostname = "concourse"
  
  config.vbguest.allow_downgrade = true

  # For whatever reason, Concourse *needs* a btrfs volume.
  config.vm.disk :disk, size: "10GB", name: "var-lib-docker"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "5120"
    vb.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
    vb.customize ["modifyvm", :id, "--uartmode1", "file", File::NULL]
    # vb.gui = true
  end

  config.vm.network "public_network", use_dhcp_assigned_default_route: true

  config.vm.provision "bootstrap", type: "shell", keep_color: false, run: "once", path: "provision/bootstrap.sh"
  config.vm.provision "validate", type: "shell", keep_color: false, run: "always", path: "provision/validate.sh"
end
