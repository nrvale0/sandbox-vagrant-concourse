#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


function packages_rinstall () {
    echo 'Installing required packages...'
    if ! command -v docker-compose 2>&1 > /dev/null; then
	(set -x;
	 export DEBIAN_FRONTEND=noninteractive;
	 apt-get update &&
             apt-get install -y \
		     tmux \
		     htop \
		     bmon \
		     mosh \
		     git \
		     docker.io \
		     docker-compose)
    fi

    if ! command -v inspec 2>&1 > /dev/null; then
	echo "Installing InSpec for validation testing..."
	(set -x;
	 wget -q -c -O /tmp/inspec-install.sh https://omnitruck.chef.io/install.sh && \
	     chmod +x /tmp/inspec-install.sh && \
	     /tmp/inspec-install.sh -P inspec)
    fi
}


function concourse_download() {
    echo "Downloading concourse..."
    if ! [ -d /opt/concourse-docker ]; then
	(set -x; 
	 mkdir -p /opt && \
	     cd /opt && \
	     git clone https://github.com/concourse/concourse-docker)
    else
	(set -x;
	 cd /opt/concourse-docker && \
	     git clean -xdf && \
	     git pull)
    fi
}


function concourse_prep() {
    echo "Prepping concourse..."
    if ! grep 'http://concourse' /opt/concourse-docker/docker-compose.yml; then
	(set -x;
	 sed -i -e 's/localhost/concourse/g' /opt/concourse-docker/docker-compose.yml)
    fi
}


function concourse_run() {
    echo "Running concourse..."
    (set -x;
     cd /opt/concourse-docker && \
	 ./keys/generate && \
     docker-compose down || true && \
	     docker-compose up -d)
	     
}
 

function docker_storage_config() {
    echo "Setting up btrfs volume for Concourse/Docker..."
    if ! mount | grep /dev/sdc 2>&1 > /dev/null; then
	(set -x;
	 mkfs.btrfs -f -L var-lib-docker /dev/sdc && \
	     mkdir -p /var/lib/docker && \
	     echo "/dev/sdc    /var/lib/docker    btrfs    defaults    0 0" >> /etc/fstab && \
	     mount -a)
    fi

    if ! -f /etc/docker/daemon.json; then
	(set -x;
	 mkdir -p /etc/docker && \
	 echo '{ "storage-driver": "btrfs" }' > /etc/docker/daemon.js)
    fi
}


function main () {
    docker_storage_config
    packages_install
    concourse_download
    concourse_prep
    concourse_run
}

main
