#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


function main () {
    echo 'Validating the environment...'
    (set -x;
     inspec detect --chef-license=accept && \
	 sleep 5 && \
	 echo "CONCOURSE_LOG_LEVEL=info" > .env && \
	 inspec exec /vagrant/provision/validate.d/inspec.d)
}


main
