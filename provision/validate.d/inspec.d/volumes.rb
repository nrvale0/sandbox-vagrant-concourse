describe mount('/var/lib/docker') do
  it { should be_mounted }
  its('type') { should eq 'btrfs' }
end

describe file('/etc/docker/daemon.js') do
  it { should exist }
end
