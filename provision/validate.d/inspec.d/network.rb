describe port(8080) do
  it { should be_listening }
end

describe http("http://localhost:8080") do
  its('status') { should eq 200 }
end

describe host('concourse') do
  it { should be_resolvable }
end
