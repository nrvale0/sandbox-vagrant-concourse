Vagrant environment for hosting a simple Concourse CI environment using the GH:concourse/concourse-docker repo.


# Requirements


## Software

-   Vagrant >= 2.2.17 (w/ experimental extra disk support)
-   VirtualBox 6.1.x
-   vagrant-vbguest plugin (`vagrant plugin install vagrant-vbguest`)


## Network

You'll need a DHCP server which adds registered names to local DNS as Concourse is configured with `CONCOURSE_EXTERNAL_URL: http://concourse:8080` and Vagrant is configured for `public_network`.


# Usage

```shell
$ vagrant up
```

```shell
$ xdg-open http://concourse:8080
```

The credentials are the default credentials from the GH:concourse/concourse-docker repo: 'test/test'.